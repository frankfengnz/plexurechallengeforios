//
//  AppDelegate.m
//  PlexureChallenge
//
//  Created by frankfeng on 4/05/2019.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "SettingsViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    RootViewController *root = [[RootViewController alloc]init];
    self.window.rootViewController = root;
    
    SettingsViewController *Obj = [[SettingsViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:Obj];
    nav.navigationBar.barStyle = UIBarStyleBlackOpaque;
    [self.window addSubview:nav.view];

    [self.window makeKeyAndVisible];
    return YES;
}

@end
