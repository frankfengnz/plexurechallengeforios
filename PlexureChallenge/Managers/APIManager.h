//
//  APIManager.h
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, NetworkRequestHTTPMethod) {
    NetworkRequestHTTPMethodGET,
    NetworkRequestHTTPMethodPOST,
    NetworkRequestHTTPMethodPUT
};

@interface APIManager : NSObject

+ (APIManager *)sharedInstance;

- (void)getDataFromSource:(void (^)(NSArray * stores))success
              failure:(void (^)(NSError* error, NSObject *errorObject))failure;

@end
