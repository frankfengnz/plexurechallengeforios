//
//  APIManager.m
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "APIManager.h"
#import "Store.h"

double const kNetworkRequestTimeout = 60.0f;
NSInteger const NetworkingCustomErrorMessage = 10000;
NSInteger const NetworkingAPIErrorMessage = 10100;

static const NSString *kAPIURL = @"https://bitbucket.org/YahiaRagaePlex/plexure-challenge/raw/449a2452c03961d5d1a094af524148cc345523db";

@implementation APIManager


+ (NSString *)httpMethodToString:(NetworkRequestHTTPMethod)httpMethod {
    return httpMethod == NetworkRequestHTTPMethodGET ? @"GET" : httpMethod == NetworkRequestHTTPMethodPUT ? @"PUT" : @"POST";
}

+ (NSString *)queryStringFromParams:(NSDictionary *)params httpMethod:(NetworkRequestHTTPMethod)httpMethod {
    NSMutableString * queryString = nil;
    NSArray *keys = [params allKeys];
    if ([keys count] > 0) {
        for (id key in keys) {
            id value = [params objectForKey:key];
            if (queryString == nil) {
                queryString = [[NSMutableString alloc] init];
                [queryString appendFormat: httpMethod == NetworkRequestHTTPMethodGET ? @"?" : @""];
            }
            else {
                [queryString appendFormat:@"&"];
            }
            if (nil != key && nil != value) {
                [queryString appendFormat:@"%@=%@", key, value];
            }
            else if (nil != key) {
                [queryString appendFormat:@"%@", key];
            }
        }
    }
    return queryString;
}

+ (NSError*)errorWithUrl:(NSString*)url
                    data:(NSData*)data
                    type:(NSString*)type
                    body:(NSString *)body
                 message:(NSString*)message
                    code:(NSInteger)code {
    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *errorDict = @{
                                @"url" : url,
                                @"type" : type,
                                @"query" : body ? body : @"",
                                @"response" : response,
                                @"message" : message
                                };
    NSLog(@"Error: %@", errorDict);
    return [NSError errorWithDomain:type code:code userInfo:errorDict];
}

+ (NSString *)changeJsonStringToTrueJsonString:(NSString *)json
{
    NSString *validString = [json stringByReplacingOccurrencesOfString:@"(\\w+)\\s*:([^A-Za-z0-9_])"
                                                            withString:@"\"$1\":$2"
                                                               options:NSRegularExpressionSearch
                                                                 range:NSMakeRange(0, [json length])];
    validString = [validString stringByReplacingOccurrencesOfString:@"([:\\[,\\{])'"
                                                         withString:@"$1\""
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    validString = [validString stringByReplacingOccurrencesOfString:@"'([:\\],\\}])"
                                                         withString:@"\"$1"
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
        validString = [validString stringByReplacingOccurrencesOfString:@"([:\\[,\\{])(\\w+)\\s*:"
                                                         withString:@"$1\"$2\":"
                                                            options:NSRegularExpressionSearch
                                                              range:NSMakeRange(0, [validString length])];
    return validString;
}
    
+ (void)makeRequestTo:(NSString *)urlAsString
             withType:(NetworkRequestHTTPMethod)httpMethod
           withParams:(NSDictionary *)params
             sendJSON:(BOOL)sendJSON
              success:(void (^)(NSDictionary *, NSInteger))success
              failure:(void (^)(NSError *, NSInteger, NSDictionary *))failure
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"--> Calling URL: %@", urlAsString);
        NSString * httpMethodString = [APIManager httpMethodToString:httpMethod];
        NSString * bodyString = [APIManager queryStringFromParams:params httpMethod:httpMethod];
        NSString * urlString = [urlAsString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL * url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
        [urlRequest setTimeoutInterval:kNetworkRequestTimeout];
        [urlRequest setHTTPMethod:httpMethodString];
        if (sendJSON) {
            [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        }
        
        if(bodyString != nil) {
            NSLog(@"Body: %@", bodyString);
            NSMutableData *body = [NSMutableData data];
            [body appendData:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
            [urlRequest setHTTPBody:body];
        }
        
        NSHTTPURLResponse *response = nil;
        NSError *error = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
        if ([data length] >0) {
            NSString* myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            myString = [APIManager changeJsonStringToTrueJsonString:myString];
            NSData *dataUTF = [myString dataUsingEncoding:NSUTF8StringEncoding];
            NSError *e = nil;
            id jsonObject = [NSJSONSerialization JSONObjectWithData:dataUTF options:NSJSONReadingAllowFragments|NSJSONReadingMutableContainers error:&e];
            BOOL jsonIsValid = [NSJSONSerialization isValidJSONObject:jsonObject];
            if(jsonIsValid) {
                if (error == nil) {
                    NSDictionary * responseDict = (NSDictionary *)jsonObject;
                    if (response.statusCode >= 300) {
                        NSError *error = [APIManager errorWithUrl:urlString data:data type:httpMethodString body:bodyString message:@"API returned unhandled message" code:NetworkingAPIErrorMessage];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            failure(error, response.statusCode, responseDict);
                        });
                    }
                    else {
                        if (success) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSLog(@"<-- %@ statusCode:%d, response:%@",urlAsString, (int)response.statusCode, responseDict);
                                success(responseDict, response.statusCode);
                            });
                        }
                    }
                }
                else {
                    if (failure) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            failure(error, response.statusCode, jsonObject);
                        });
                    }
                }
            } else {
                NSError *error = [APIManager errorWithUrl:urlString data:data type:httpMethodString body:bodyString message:@"wrong JSON" code:NetworkingCustomErrorMessage];
                if (failure) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        failure(error, response.statusCode, nil);
                    });
                }
            }
        }
        else if ([data length] == 0 && error == nil) {
            NSError *error = [APIManager errorWithUrl:urlString data:data type:httpMethodString body:bodyString message:@"empty response" code:NetworkingCustomErrorMessage];
            if (failure) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(error, response.statusCode, nil);
                });
            }
            
        }
        else if (error != nil) {
            NSLog(@"Error: %@", error.userInfo);
            if (failure) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    failure(error, response.statusCode, nil);
                });
            }
        }
    });
}

+ (APIManager *)sharedInstance {
    static dispatch_once_t onceToken;
    static APIManager *shared = nil;
    dispatch_once(&onceToken, ^{
        shared = [[APIManager alloc] init];
    });
    return shared;
}

- (NSString *)urlWithTail:(NSString *)tail {
    if (tail == nil)
        return nil;
    return [NSString stringWithFormat:@"%@/%@", kAPIURL, tail];
}

- (void)processError:(NSError *)error response:(NSDictionary *)response statusCode:(NSInteger)statusCode failure:(void (^)(NSError* error, NSObject *errorObject))failure {
    if (failure) {
        failure(error, nil);
    }
}

- (void)getDataFromSource:(void (^)(NSArray * stores))success
                     failure:(void (^)(NSError* error, NSObject *errorObject))failure {
    NSString * url = [self urlWithTail:@"data.json"];
    [APIManager makeRequestTo:url withType:NetworkRequestHTTPMethodGET withParams:nil sendJSON:NO success:^(NSDictionary *responseObject, NSInteger statusCode) {
        NSMutableArray * stores = [[NSMutableArray alloc] initWithCapacity:[responseObject count]];
        for (NSDictionary * store in responseObject) {
            [stores addObject:[[Store alloc] initWithJSON:store]];
        }
        if (success) {
            success([NSArray arrayWithArray:stores]);
        }
    } failure:^(NSError *error, NSInteger statusCode, NSDictionary *responseObject) {
        [self processError:error response:responseObject statusCode:statusCode failure:failure];
    }];
}


@end
