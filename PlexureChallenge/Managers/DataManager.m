//
//  DataManager.m
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "DataManager.h"

static NSString * const kStores = @"kStores";
static NSString * const kBadgeNumber = @"kBadgeNumber";
static NSString * const kDistanceBoundary = @"kDistanceBoundary";

@interface DataManager () {
    NSUserDefaults *_defaults;
}
@end

@implementation DataManager

+ (DataManager *)sharedInstance {
    static dispatch_once_t onceToken;
    static DataManager *shared = nil;
    dispatch_once(&onceToken, ^{
        shared = [[DataManager alloc] init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _defaults = [NSUserDefaults standardUserDefaults];
        [self loadDataFromCache];
    }
    return self;
}

- (void)loadDataFromCache {
    _stores = [_defaults objectForKey:kStores] ? [NSKeyedUnarchiver unarchiveObjectWithData:[_defaults objectForKey:kStores]] : nil;
    _number = [_defaults integerForKey:kBadgeNumber] ? [_defaults integerForKey:kBadgeNumber] : 0;
    _distance = [_defaults doubleForKey:kDistanceBoundary] ? [_defaults doubleForKey:kDistanceBoundary] : 80.0;
}

- (void)setStores:(NSArray *)stores {
    _stores = stores;
    if (stores == nil) {
        [_defaults removeObjectForKey:kStores];
        [_defaults synchronize];
    }
    else {
        [self saveUser];
    }
}

- (void)saveUser {
    NSData * userData = [NSKeyedArchiver archivedDataWithRootObject:_stores];
    [_defaults setObject:userData forKey:kStores];
    [_defaults synchronize];
}

- (NSArray *)getStores {
    return _stores;
}

- (void)setBadgeNumber:(NSInteger)number {
    _number = number;
    [_defaults setInteger:_number forKey:kBadgeNumber];
    [_defaults synchronize];
}

- (void)addBadgeNumber:(NSInteger)number {
    NSInteger num =_number + number;
    if (num < 0)
        num = 0;
    [self setBadgeNumber:num];
}

- (NSInteger)getBadgeNumber {
    return _number;
}

- (void)setDistanceBoundary:(double)distance {
    _distance = distance;
    [_defaults setDouble:_distance forKey:kDistanceBoundary];
    [_defaults synchronize];
}

- (double)getDistanceBoundary {
    return _distance;
}

@end
