//
//  DataManager.h
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

@property (strong, nonatomic) NSArray *stores;
@property (assign, nonatomic) NSInteger number;
@property (assign, nonatomic) double distance;

+ (DataManager *)sharedInstance;
- (void)setStores:(NSArray *)stores;
- (NSArray *)getStores;
- (void)setBadgeNumber:(NSInteger)number;
- (void)addBadgeNumber:(NSInteger)number;
- (NSInteger)getBadgeNumber;
- (void)setDistanceBoundary:(double)distance;
- (double)getDistanceBoundary;

@end
