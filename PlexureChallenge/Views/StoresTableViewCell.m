//
//  StoresTableViewCell.m
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "StoresTableViewCell.h"
#import "DataManager.h"


@interface StoresTableViewCell () 

@property (nonatomic, strong) Store *store;

@property (weak, nonatomic) IBOutlet UILabel *storeNameView;
@property (weak, nonatomic) IBOutlet UILabel *storeDistanceView;
@property (weak, nonatomic) IBOutlet UISwitch *storeFavouriteView;
@property (weak, nonatomic) IBOutlet UITextView *storeAddressView;
@property (weak, nonatomic) IBOutlet UITextView *storeFeatureListView;
@property (weak, nonatomic) IBOutlet UILabel *storeFavouriteLabelView;
@property (weak, nonatomic) IBOutlet UIButton *removeView;
@property (weak, nonatomic) IBOutlet UIView *storeContentView;

@end

@implementation StoresTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.storeFavouriteView addTarget:self action:@selector(toggleFavouriteSwitch) forControlEvents:UIControlEventValueChanged];
    [self.removeView.layer setBorderWidth:1.0f];
    [self.removeView addTarget:self action:@selector(pressFavouriteRemove) forControlEvents:UIControlEventTouchDown];
}

- (void)setItemValues:(Store *)store type:(NSInteger)type {
    self.store = store;
    if (store == nil)
        return;
    self.storeNameView.text = store.name;
    if (type == 1)
        self.storeAddressView.text = store.address;
    else
        [self performSelector:@selector(delaySetAddress) withObject:nil afterDelay:2.0];
    NSString *dis = [NSString stringWithFormat:@"%.3fkm", store.distance/1000];
    self.storeDistanceView.text = dis;
    self.storeFeatureListView.text = store.features;
    [self.storeFavouriteView setOn:store.favourite];
    if (type == 0) {
        [self.removeView setHidden:YES];
        [self.storeFavouriteView setHidden:NO];
        [self.storeFavouriteLabelView setHidden:NO];
        double distanceBoundary = [[DataManager sharedInstance] getDistanceBoundary];
        if (store.distance > distanceBoundary*1000) {
            [self.storeContentView setBackgroundColor:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0]];
        } else {
            [self.storeContentView setBackgroundColor:[UIColor clearColor]];
        }
    } else {
        [self.removeView setHidden:NO];
        [self.storeFavouriteView setHidden:YES];
        [self.storeFavouriteLabelView setHidden:YES];
    }
}

- (void)delaySetAddress {
    self.storeAddressView.text = self.store.address;
}

- (void)toggleFavouriteSwitch {
    [[DataManager sharedInstance] addBadgeNumber:[self.storeFavouriteView isOn] ? 1 : -1];
    if (self.delegate) {
        [self.delegate favouriteDidChange:self.store.mId on:[self.storeFavouriteView isOn]];
    }
}

- (void)pressFavouriteRemove {
    if (self.delegate) {
        [self.delegate favouriteDidChange:self.store.mId on:NO];
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
