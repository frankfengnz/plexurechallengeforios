//
//  StoresTableViewCell.h
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Store.h"

@protocol StoresTableViewCellDelegate <NSObject>

- (void)favouriteDidChange:(NSInteger)mid on:(BOOL)on;

@end

@interface StoresTableViewCell : UITableViewCell

@property (nonatomic, weak) id <StoresTableViewCellDelegate> delegate;

- (void)setItemValues:(Store *)store type:(NSInteger)type;

@end
