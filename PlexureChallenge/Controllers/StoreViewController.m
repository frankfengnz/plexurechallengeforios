//
//  StoreViewController.m
//  PlexureChallenge
//
//  Created by frankfeng on 4/05/2019.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "StoreViewController.h"
#import "StoresTableViewCell.h"
#import "APIManager.h"
#import "DataManager.h"
#import "CustomAlertView.h"

static const NSInteger kSortStart = 0;
static const NSInteger kFilterStart = 100;
static NSArray *kFilterButtons;

@interface StoreViewController () <StoresTableViewCellDelegate, CustomAlertViewDelegate>

@property (assign, nonatomic) NSInteger viewType;
@property (nonatomic, strong) NSArray *stores;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) CustomAlertView *objCustomAlert;
@property (nonatomic, strong) NSString *filterBy;
@property (nonatomic, assign) NSInteger sortBy;

@end

@implementation StoreViewController {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    kFilterButtons = @[@"FREE_WIFI", @"DRIVETHR", @"MCCAFE", @"BP", @"MCADVENT", @"BF", @"TABLE_DELIVERY", @"No filter"];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    if (self.viewType == 0) {
        UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithTitle:@"Filter" style:UIBarButtonItemStylePlain target:self action:@selector(filterPressed)];
        UIBarButtonItem *sortButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort" style:UIBarButtonItemStylePlain target:self action:@selector(sortPressed)];
        self.navigationController.navigationBar.topItem.rightBarButtonItem = filterButton;
        self.navigationController.navigationBar.topItem.leftBarButtonItem = sortButton;
        self.navigationController.navigationBar.translucent = NO;
    }
}

- (void)initPost:(NSInteger)type {
    self.viewType = type;
}

- (void)setPageData:(NSArray *)stores {
    self.stores = stores;
    if (self.tableView != nil)
        [self.tableView reloadData];
    if (self.viewType == 0) {
        self.title = @"Stores";
        [self.refreshControl endRefreshing];
    } else {
        self.title = @"Favourites";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)filterPressed {
    [self showFilterView];
}

- (void)sortPressed {
    [self showSortView];
}

- (void)refreshTable {
    if (self.delegate) {
        [self.delegate refreshData];
    }
    //[self.tableView reloadData];
    //[self.refreshControl endRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.tableView = tableView;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height - 100;
    self.tableView.frame = CGRectMake(0, 0, width, height);
    
    if (self.viewType == 0) {
        if (@available(iOS 10.0, *)) {
            self.tableView.refreshControl = self.refreshControl;
        } else {
            [self.tableView addSubview:self.refreshControl];
        }
    }
    if (self.stores == nil)
        return 0;
    return [self.stores count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 270;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *storesTableIdentifier = @"kStoresTableIndentifier";
    tableView.dataSource = self;
    tableView.delegate = self;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:storesTableIdentifier];
    
    if (cell == nil) {
        NSArray *cellnib = [[NSBundle mainBundle] loadNibNamed:@"StoresTableViewCell" owner:self options:nil];
        cell = (StoresTableViewCell *)[cellnib objectAtIndex:0];
        [(StoresTableViewCell *)cell setDelegate:self];
        [(StoresTableViewCell *)cell setItemValues:self.stores?self.stores[indexPath.row]:nil type:self.viewType];

    }
    return cell;
}

- (void)favouriteDidChange:(NSInteger)mid on:(BOOL)on {
    if (self.delegate) {
        [self.delegate favouriteDidChange:mid on:on];
    }
}

- (void)showSortView {
    [self showCustomAlertView:@"Sort by distance in:" buttons:@[ @"Do not sort", @"Ascending order", @"Descending order"] buttonTagStart:kSortStart];
}

- (void)showFilterView {
    [self showCustomAlertView:@"Filter by feature list of:" buttons:kFilterButtons buttonTagStart:kFilterStart];
}

- (void)showCustomAlertView:(NSString *)title buttons:(NSArray *)buttons buttonTagStart:(NSInteger)buttonTagStart{
    if (self.objCustomAlert == nil) {
        self.objCustomAlert = [[CustomAlertView alloc] init];
    }
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 290, 90)];
    UILabel *titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, containerView.frame.size.width, 30)];
    titleView.center = CGPointMake(containerView.frame.size.width / 2, containerView.frame.size.height / 2);
    [titleView setTextAlignment:NSTextAlignmentLeft];
    titleView.text = title;
    [titleView setTextColor:UIColor.blackColor];
    [containerView addSubview:titleView];
    self.objCustomAlert.containerView = containerView;
    self.objCustomAlert.delegate = self;

    self.objCustomAlert.buttonTitles = buttons;
    self.objCustomAlert.buttonTagStart = buttonTagStart;
    self.objCustomAlert.showButtonRadius = true;
    self.objCustomAlert.buttonOrientation = ShowButtonsDirectionVertical;
    self.objCustomAlert.alertViewBgColor = UIColor.whiteColor;
    self.objCustomAlert.buttonBackgroundColors = [[NSArray alloc] initWithObjects:@"#808080", nil];
    [self.objCustomAlert show];
}

//MARK:- Delegate Methods

// Handle CustomAlertView button touches
- (void)customDialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex >= kFilterStart) {
        NSInteger index = buttonIndex - kFilterStart;
        if (index >= [kFilterButtons count]-1) {
            self.filterBy = nil;
        } else {
            self.filterBy = kFilterButtons[index];
        }
    } else {
        self.sortBy = buttonIndex;
    }
    [self.objCustomAlert close];
    if (self.delegate) {
        [self.delegate reloadSortedFilteredPage:self.filterBy sort:self.sortBy];
    }
}

/*
- (IBAction)buttonPressed:(id)sender {
    if (self.viewType == 0) {
        FavouriteViewController *fav = [[FavouriteViewController alloc] init];
        [self.navigationController pushViewController:fav animated:true];
    } else {
        StoreViewController *store = [[StoreViewController alloc] init];
        [self.navigationController pushViewController:store animated:true];
    }
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
