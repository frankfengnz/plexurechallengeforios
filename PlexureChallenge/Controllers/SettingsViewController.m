//
//  SettingsViewController.m
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "SettingsViewController.h"
#import "DataManager.h"

@interface SettingsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *distanceView;
@property (weak, nonatomic) IBOutlet UIButton *setButtonView;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    [self.setButtonView.layer setBorderWidth:1.0f];
    [self.setButtonView addTarget:self action:@selector(pressToSet) forControlEvents:UIControlEventTouchDown];
    double distanceBoundary = [[DataManager sharedInstance] getDistanceBoundary];
    [self.distanceView setText:[NSString stringWithFormat:@"%.3f", distanceBoundary]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pressToSet {
    double dis = [self.distanceView.text doubleValue];
    [[DataManager sharedInstance] setDistanceBoundary:dis];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
