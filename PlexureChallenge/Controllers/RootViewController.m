

//
//  RootViewController.m
//  PlexureChallenge
//
//  Created by frankfeng on 4/05/2019.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "RootViewController.h"
#import "StoreViewController.h"
#import "SettingsViewController.h"
#import "APIManager.h"
#import "DataManager.h"
#import "Store.h"

@interface RootViewController ()<UITabBarControllerDelegate, StoreViewControllerDelegate>

@property (nonatomic,strong)UITabBarController *tabBarController;
@property (nonatomic, strong) UINavigationController *storeNav;
@property (nonatomic, strong) UINavigationController *favouriteNav;
@property (nonatomic, strong) UINavigationController *settingsNav;
@property (nonatomic, strong) StoreViewController *storeVC;
@property (nonatomic, strong) StoreViewController *favouriteVC;
@property (nonatomic, strong) SettingsViewController *settingsVC;

@property (nonatomic, strong) NSArray *stores;

@end

@implementation RootViewController

- (void)viewDidLoad {

    [super viewDidLoad];
    
    self.stores = [[DataManager sharedInstance] getStores];
    [self getDataFromSource];
}

- (void)loadViews:(NSArray *) stores {
    self.tabBarController = [[UITabBarController alloc]init];
    self.tabBarController.delegate = self;
    
    self.storeVC = [[StoreViewController alloc]init];
    [self.storeVC setDelegate:self];
    [self.storeVC initPost:0];
    [self.storeVC setPageData:stores];
    self.storeNav = [[UINavigationController alloc]initWithRootViewController:self.storeVC];
    self.storeNav.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemContacts tag:0];
    [self.storeNav.tabBarItem setValue:@"Stores" forKey:@"internalTitle"];

    self.favouriteVC = [[StoreViewController alloc]init];
    [self.favouriteVC setDelegate:self];
    [self.favouriteVC initPost:1];
    [self.favouriteVC setPageData:[self getFavouriteStores:stores]];
    self.favouriteNav = [[UINavigationController alloc]initWithRootViewController:self.favouriteVC];
    self.favouriteNav.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:1];
    [self.favouriteNav.tabBarItem setValue:@"Favourites" forKey:@"internalTitle"];
 
    self.settingsVC = [[SettingsViewController alloc]init];
    //[self.settingsVC setDelegate:self];
    self.settingsNav = [[UINavigationController alloc]initWithRootViewController:self.settingsVC];
    self.settingsNav.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemBookmarks tag:2];
    [self.settingsNav.tabBarItem setValue:@"Settings" forKey:@"internalTitle"];

    self.tabBarController.viewControllers = [NSArray arrayWithObjects:self.storeNav, self.favouriteNav, self.settingsNav, nil];
    [self.view addSubview:self.tabBarController.view];

    [self showBadge];
    
}

- (void)getDataFromSource {
    [[APIManager sharedInstance] getDataFromSource:^(NSArray *stores) {
        if (![self compareStores:stores to:self.stores])
            self.stores = stores;
        [self loadViews: self.stores];
        [[DataManager sharedInstance] setStores:self.stores];
        NSLog(@"Get data uccessfully.");
    } failure:^(NSError *error, NSObject *errorObject) {
        NSLog(@"Get data failed.");
    }];
}

- (NSArray *)getFavouriteStores:(NSArray *) stores {
    NSMutableArray * items = [[NSMutableArray alloc] initWithCapacity:[stores count]];
    for (Store * store in stores) {
        if (store.favourite)
            [items addObject:store];
    }
    return [NSArray arrayWithArray:items];
}

- (void)showBadge {
    NSInteger number = [[DataManager sharedInstance] getBadgeNumber];
    if (number > 0)
        [self.favouriteNav.tabBarItem setBadgeValue:[NSString stringWithFormat:@"%d", (int)number]];
    else
        [self.favouriteNav.tabBarItem setBadgeValue:nil];
}

- (Store *)findStoreById:(NSInteger)mid {
    for (Store *store in self.stores) {
        if (store.mId == mid) {
            return store;
        }
    }
    return nil;
}

- (BOOL)compareStores:(NSArray *)new to:(NSArray *)old {
    if (old == nil || [old count] <= 0)
        return NO;
    if (new == nil)
        return YES;
    for (Store *store in old) {
        Store *s = [self findStoreById:store.mId];
        if (s == nil)
            return NO;
    }
    return YES;
}

- (void)favouriteDidChange:(NSInteger)mid on:(BOOL)on {
    Store *store = [self findStoreById:mid];
    if (store == nil)
        return;
    store.favourite = on;
    [[DataManager sharedInstance] setStores:self.stores];
    [self showBadge];
    [self.favouriteVC setPageData:[self getFavouriteStores:self.stores]];
}

- (void)refreshData {
    [self getDataFromSource];
}

- (void)reloadSortedFilteredPage:(NSString *)filter sort:(NSInteger)sort {
    NSMutableArray *stores = [[NSMutableArray alloc] initWithCapacity:[self.stores count]];
    for (Store *store in self.stores) {
        if (filter == nil || [store.features containsString:filter]) {
            [stores addObject:store];
        }
    }
    NSArray *storesDone;
    if (sort > 0) {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:sort==1];
        storesDone = [stores sortedArrayUsingDescriptors:@[sortDescriptor]];
    } else {
        storesDone = [NSArray arrayWithArray:stores];
    }
    [self.storeVC setPageData:storesDone];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    if ([viewController isEqual:self.favouriteNav]) {
        [[DataManager sharedInstance] setBadgeNumber:0];
        [self showBadge];
        [self.favouriteVC setPageData:[self getFavouriteStores:self.stores]];
    } else if ([viewController isEqual:self.storeNav]) {
        [self.storeVC setPageData:self.stores];
    }
}

@end
