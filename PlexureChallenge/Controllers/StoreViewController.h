//
//  StoreViewController.h
//  PlexureChallenge
//
//  Created by frankfeng on 4/05/2019.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"

@protocol StoreViewControllerDelegate <NSObject>

- (void)favouriteDidChange:(NSInteger)mid on:(BOOL)on;
- (void)refreshData;
- (void)reloadSortedFilteredPage:(NSString *)filter sort:(NSInteger)sort;

@end

@interface StoreViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id <StoreViewControllerDelegate> delegate;


- (void)initPost:(NSInteger)type;
- (void) setPageData:(NSArray *)stores;

@end
