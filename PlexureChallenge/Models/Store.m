//
//  Store.m
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import "Store.h"

@implementation Store

- (instancetype)initWithJSON:(NSDictionary *)json {
    self =  [super init];
    if (self) {
        self.mId = [json[@"id"] integerValue];
        self.name = json[@"name"];
        self.address = json[@"address"];
        //self.address = [[NSString alloc] initWithData:json[@"address"] encoding:NSUTF8StringEncoding];
        self.distance = [json[@"distance"] doubleValue];
        self.latitude = [json[@"latitude"] doubleValue];
        self.longitude = [json[@"longitude"] doubleValue];
        //NSMutableArray * items = [[NSMutableArray alloc] initWithCapacity:[json[@"featureList"] count]];
        self.features = @"";
        for (NSString * feat in json[@"featureList"]) {
            //[items addObject:feat];
            if ([self.features length] > 0)
                self.features = [NSString stringWithFormat:@"%@, %@", self.features, feat];
            else
                self.features = feat;
        }
        //self.featureList = [NSArray arrayWithArray:items];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_address forKey:@"address"];
    [encoder encodeObject:_features forKey:@"features"];
    [encoder encodeInteger:_mId forKey:@"mId"];
    [encoder encodeDouble:_distance forKey:@"distance"];
    [encoder encodeDouble:_latitude forKey:@"latitude"];
    [encoder encodeDouble:_longitude forKey:@"longitude"];
    [encoder encodeBool:_favourite forKey:@"favourite"];
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (self) {
        self.name = [decoder decodeObjectForKey:@"name"];
        self.address = [decoder decodeObjectForKey:@"address"];
        self.features = [decoder decodeObjectForKey:@"features"];
        self.mId = [decoder decodeIntegerForKey:@"mId"];
        self.distance = [decoder decodeDoubleForKey:@"distance"];
        self.latitude = [decoder decodeDoubleForKey:@"latitude"];
        self.longitude = [decoder decodeDoubleForKey:@"longitude"];
        self.favourite = [decoder decodeBoolForKey:@"favourite"];
    }
    return self;
}

@end
