//
//  Store.h
//  PlexureChallenge
//
//  Created by Frank Feng on 4/05/19.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Store : NSObject

@property (assign, nonatomic) NSUInteger mId;
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) NSString * address;
@property (assign, nonatomic) double distance;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
//@property (strong, nonatomic) NSArray * featureList;
@property (strong, nonatomic) NSString * features;
@property (assign, nonatomic) BOOL favourite;

- (instancetype)initWithJSON:(NSDictionary *)json;

@end
