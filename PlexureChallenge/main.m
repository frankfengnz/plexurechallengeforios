//
//  main.m
//  PlexureChallenge
//
//  Created by frankfeng on 4/05/2019.
//  Copyright © 2019 FrankFeng. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
