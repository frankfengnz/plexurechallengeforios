Project of Mobile technical challenge of Plexure

Introduce:
This project is for Mobile technical challenge of Plexure. The requirement is here:
https://bitbucket.org/YahiaRagaePlex/plexure-challenge/src/master/Mobile_Tech_Challenge.md

Running environment:
Xcode 9.2 was used as IDE. The application works well from iPhone 5s to iPhone X, and the iPad, iPad Pro as well.

Features achieved:
1. The entire basic feature set in the requirement, including UI; networking; data handing and multithreading.
2. five additional features set as below:
    a. a filter of feature list property;
    b. a sort tool by distance in ascending/descending/none ways; 
    c. use grey background colour if the distance is more than 80km (can be changed in settings);
    d. delay showing the address property;
    e. add a badge to the tab bar of favourites list item to indicate the number of new favourite stores;
